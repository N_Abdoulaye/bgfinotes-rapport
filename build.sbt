name := """bgfinotes-rapport"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "mysql" % "mysql-connector-java" % "5.1.27",
  "com.typesafe.play" %% "play-mailer" % "2.4.0",
  "commons-dbutils" % "commons-dbutils" % "1.6",
  "org.apache.commons" % "commons-lang3" % "3.3.2",
  "org.apache.poi" % "poi" % "3.13",
  "org.apache.poi" % "poi-ooxml" % "3.9",
  "commons-net" % "commons-net" % "3.2"
)
