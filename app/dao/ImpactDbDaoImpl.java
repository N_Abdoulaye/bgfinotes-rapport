package dao;
import play.Logger;
import play.db.DB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by ghambyte on 6/26/16.
 */
public class ImpactDbDaoImpl implements ImpactDbDAO {
    @Override
    public boolean populateDB(String num, String agec, String coup, String dev, String val,
                              String aged, String ages, String agep, String ageo, String agel,
                              String dou, String dmo, String dars, String dass, String dara,
                              String dav, String dap, String dop, String dlop, String utirs,
                              String utiss, String utira, String utiv, String utip, String utio,
                              String utilo, String typp, String ncps, String sufs, String devs,
                              String cli, String moto, String motl, String eve, String ctr, String atrf) throws SQLException {
        Connection connection = DB.getConnection();
        Statement stm = null;
        ResultSet rs =  null;
        StringBuilder builder = new StringBuilder("SELECT * FROM checkload WHERE num ='" + num + "'");
        stm = connection.createStatement();
        rs = stm.executeQuery(builder.toString());
        Logger.debug("starting requete ....."+ builder);
        StringBuilder builder1 = new StringBuilder("UPDATE checkload SET agec='"+ agec +"', coup='"+ coup +"', sufs='"+ sufs +"', " +
                "dev='"+ dev +"', val='"+ val +"', aged='"+ aged +"', ages='"+ ages +"', agep='"+ agep +"', ageo='"+ ageo +"', " +
                "agel='"+ agel +"', dou='"+ dou +"', dmo='"+ dmo +"', dars='"+ dars +"', dass='"+ dass +"', dara='"+ dara +"'," +
                " dav='"+ dav +"', dap='"+ dap +"', dop='"+ dop +"', dlop='"+ dlop +"', utirs='"+ utirs +"', utiss='"+ utiss +"', " +
                "utira='"+ utira +"', utiv='"+ utiv +"', utip='"+ utip +"', utio='"+ utio +"', cli='"+ cli +"', moto='"+ moto +"', " +
                "motl='"+ motl +"', eve='"+ eve +"', ctr='"+ ctr +"', atrf='"+ atrf +"' WHERE num='" + num +"'");
        StringBuilder builder1_1 = new StringBuilder("UPDATE travelcheck SET coupure='"+coup+"', devise='"+dev+"', " +
                "valeur='"+val+"', status='"+utilo+"', codefiliale='"+ctr+"' WHERE numerocheck='"+num+"'");

        StringBuilder builder2 = new StringBuilder("INSERT INTO checkload(num,agec,coup,dev,val,aged,ages,agep,ageo,agel,dou,dmo,dars," +
                "dass,dara,dav,dap,dop,dlop,utirs,utiss,utira,utiv,utip,utio,utilo,typp,ncps,sufs,devs,cli,moto,motl,eve,ctr,atrf) " +
                "VALUES ('"+ num + "','" + agec +"','" + coup +"','" + dev +"','" + val +"','" + aged +"','" + ages +"','"
                + agep +"','" + ageo +"','" + agel +"','" + dou +"','" + dmo +"','" + dars +"','" + dass +"','" + dara +"','"
                + dav +"','" + dap +"','" + dop +"','" + dlop +"','" + utirs +"','" + utiss +"','" + utira +"','" + utiv +"','"
                + utip +"','" + utio +"','" + utilo +"','" + typp +"','" + ncps +"','" + sufs +"','" + devs +"','" + cli +"','"
                + moto +"','" + motl +"','" + eve +"','" + ctr +"','" + atrf +"')");
        StringBuilder builder2_2 = new StringBuilder("INSERT INTO travelcheck(numerocheck,coupure,devise,valeur,status,codefiliale) VALUES('"+num+"','"+coup+"', '"+dev+"', '"+val+"', '"+atrf+"', '"+ctr+"')");
        Logger.debug("REQ " + builder2_2);
        if (rs.next()){
            Logger.debug("Enrégistrement existe!!!");
            stm = connection.createStatement();
            try {
                int retour = stm.executeUpdate(builder1.toString());
                int retour1 = stm.executeUpdate(builder1_1.toString());
                if (retour == 0) {
                    Logger.info("RETOUR UPDATE checkloader==> " + retour);
                    return false;
                }
                if (retour1 == 0) {
                    Logger.info("RETOUR UPDATE travelcheck==> " + retour);
                    return false;
                }
                Logger.info("RETOUR STATEMENT checkloader==> " + retour1);
                Logger.info("req ==> " + builder1);
                Logger.info("RETOUR STATEMENT tavelcheck==> " + retour1);
                Logger.info("req ==> " + builder1_1);

            } catch (SQLException e) {
                Logger.error("newUser SQLException " + e.getMessage());
                return false;
            } finally {
                try {
                    stm.close();
                    connection.close();

                } catch (SQLException e) {
                    Logger.error("SQLExeption " + e.getMessage());
                }
            }
            return true;
        }else{
            Logger.debug("Enrégistrement n'existe pas!!!");
            stm = connection.createStatement();
            try {
                int retour = stm.executeUpdate(builder2.toString());
                int retour1 = stm.executeUpdate(builder2_2.toString());
                if (retour == 0) {
                    Logger.info("RETOUR INSERT checkloader ==> " + retour);
                    return false;
                }
                Logger.info("RETOUR STATEMENT checkloader==> " + retour);
                Logger.info("RETOUR STATEMENT travelcheck==> " + retour1);
                Logger.info("req ==> " + builder2);
                Logger.info("req ==> " + builder2_2);

            } catch (SQLException e) {
                Logger.error("newUser SQLException " + e.getMessage());
                return false;
            } finally {
                try {
                    stm.close();
                    connection.close();

                } catch (SQLException e) {
                    Logger.error("SQLExeption " + e.getMessage());
                }
            }
            return true;
        }
    }
}
