package dao;

import play.Logger;
import play.db.DB;
import tools.Utils;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ghambyte on 6/26/16.
 */
public class JournalDaoImpl implements JournalDAO {
    @Override
    public void getJournal() {
        Connection connection = DB.getConnection();
        Statement st;
        ResultSet rs = null;
        List data = new ArrayList();
        Date date = new Date();
        DateFormat outputFormatter = new SimpleDateFormat("MM-dd-yyyy");
        String output = outputFormatter.format(date);

        try {
            st= connection.createStatement();
            String req = "SELECT * FROM journal";
            Logger.debug("iterate ############### " +req);
            rs = st.executeQuery(req);
            while (rs.next()) {
                String valeur = rs.getString("valeur");
                String compte = rs.getString("numerocompte");
                data.add(valeur + " " + " " + compte);
            }
            File file = new File("public/myFiles/rapport-"+output+"/");
            file.mkdir();
            Utils.writeToFile(data, "public/myFiles/rapport-"+output+"/_journal.txt");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
