package services;

import java.sql.SQLException;

/**
 * Created by ghambyte on 6/26/16.
 */
public interface ImpactDbService {
    boolean populateDB(String num, String agec, String coup, String dev, String val,
                       String aged, String ages, String agep, String ageo, String agel,
                       String dou, String dmo, String dars, String dass, String dara,
                       String dav, String dap, String dop, String dlop, String utirs,
                       String utiss, String utira, String utiv, String utip, String utio,
                       String utilo, String typp, String ncps, String sufsn, String devs,
                       String cli, String moto, String motl, String eve, String ctr, String atrf) throws SQLException;
}
