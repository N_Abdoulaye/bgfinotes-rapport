package services;

import dao.ImpactDbDaoImpl;

import java.sql.SQLException;

/**
 * Created by ghambyte on 6/26/16.
 */
public class ImpactDbServiceImpl implements ImpactDbService {
    @Override
    public boolean populateDB(String num, String agec, String coup, String dev, String val,
                              String aged, String ages, String agep, String ageo, String agel,
                              String dou, String dmo, String dars, String dass, String dara,
                              String dav, String dap, String dop, String dlop, String utirs,
                              String utiss, String utira, String utiv, String utip, String utio,
                              String utilo, String typp, String ncps, String sufsn, String devs,
                              String cli, String moto, String motl, String eve, String ctr, String atrf) throws SQLException {
        ImpactDbDaoImpl impactDbDao = new ImpactDbDaoImpl();
        return impactDbDao.populateDB(num, agec, coup, dev, val, aged, ages, agep, ageo, agel,
                dou, dmo, dars, dass, dara, dav, dap, dop, dlop, utirs, utiss, utira, utiv,
                utip, utio, utilo, typp, ncps, sufsn, devs, cli, moto, motl, eve, ctr, atrf);
    }
}
