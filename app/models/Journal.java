package models;

/**
 * Created by ghambyte on 6/25/16.
 */
public class Journal {

    private String value;
    private String account;

    public Journal() {
    }

    public Journal(String value, String account) {
        this.value = value;
        this.account = account;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
