package tools;
import com.sun.media.jfxmedia.logging.Logger;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import services.JournalServiceImpl;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by ghambyte on 6/26/16.
 */
public class Utils{

    public static void downloadFile(){
        String server = "ftp.domain.com";
        int port = 21;
        String user = "user";
        String pass = "pass";

        FTPClient ftpClient = new FTPClient();
        try {

            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            String remoteFile = "/test/employee.txt";
            File downloadFile = new File("public/myFiles/employee.txt");
            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile));
            boolean success = ftpClient.retrieveFile(remoteFile, outputStream);

            outputStream.close();

            if (success) {
                System.out.println("Fichier reçu avec succès");
            }else {
                System.out.println("non connecté au serveur:   "+ ftpClient.getStatus());
            }

        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void uploadFile(){
        FTPClient ftpClient = new FTPClient();
        FileInputStream fis = null;

        try {
            ftpClient.connect("ftp.domain.com");
            ftpClient.login("admin", "secret");
            Date date = new Date();
            DateFormat outputFormatter = new SimpleDateFormat("MM-dd-yyyy");
            String output = outputFormatter.format(date);
            JournalServiceImpl journalService = new JournalServiceImpl();
            journalService.getJournal();
            String filename = "public/myFiles/rapport-"+output+"/_journal.txt";
            fis = new FileInputStream(filename);
            ftpClient.storeFile(filename, fis);
            boolean success = ftpClient.storeFile(filename, fis);
            if (success) {
                System.out.println("Fichier envoyé avec succès");
            }
            ftpClient.logout();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                ftpClient.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void writeToFile(java.util.List list, String path) {
        BufferedWriter out = null;
        try {
            File file = new File(path);
            out = new BufferedWriter(new FileWriter(file, true));
            for (Object s : list) {
                out.write(s.toString());
                out.newLine();

            }
            out.close();
        } catch (IOException e) {
        }
    }
}
