import dao.ImpactDbDaoImpl;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.db.DB;
import play.libs.Akka;
import scala.concurrent.duration.FiniteDuration;
import services.ImpactDbServiceImpl;
import services.JournalServiceImpl;
import tools.Utils;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Global extends GlobalSettings {

    Statement st;

    public void onStart(Application app) {
        Logger.info("Application has started");
        receiveFile();
        //sendFile();
    }

    private void receiveFile() {
        Connection connection = DB.getConnection();
        FiniteDuration delay = FiniteDuration.create(0, TimeUnit.SECONDS);
        FiniteDuration frequency = FiniteDuration.create(1, TimeUnit.DAYS);
        ImpactDbServiceImpl impactDbDao = new ImpactDbServiceImpl();

        Akka.system().scheduler().schedule(delay, frequency, () -> {
            try {
                st= connection.createStatement();
                String fileName = "public/myFiles/Employee.txt";
                try (Scanner scanner = new Scanner(new File(fileName))) {

                    while (scanner.hasNext()){
                        String line = scanner.nextLine();
                        String [] parts = line.split("\\|");
                        String num = parts[1];
                        String agec = parts[0];
                        String coup = parts[2];
                        String dev = parts[3];
                        String val = parts[4];
                        String aged = parts[5];
                        String ages = parts[6];
                        String agep = parts[7];
                        String ageo = parts[8];
                        String agel = parts[9];
                        String dou = parts[10];
                        String dmo = parts[11];
                        String dars = parts[12];
                        String dass = parts[13];
                        String dara = parts[14];
                        String dav = parts[15];
                        String dap = parts[16];
                        String dop = parts[17];
                        String dlop = parts[18];
                        String utirs = parts[19];
                        String utiss = parts[20];
                        String utira = parts[21];
                        String utiv = parts[22];
                        String utip = parts[23];
                        String utio = parts[24];
                        String utilo = parts[25];
                        String typp = parts[26];
                        String ncps = parts[27];
                        String sufs = parts[28];
                        String devs = parts[29];
                        String cli = parts[30];
                        String moto = parts[31];
                        String motl = parts[32];
                        String eve = parts[33];
                        String ctr = parts[34];
                        String atrf = parts[35];
                        impactDbDao.populateDB(num, agec, coup, dev, val, aged, ages, agep, ageo, agel,
                                dou, dmo, dars, dass, dara, dav, dap, dop, dlop, utirs, utiss, utira, utiv,
                                utip, utio, utilo, typp, ncps, sufs, devs, cli, moto, motl, eve, ctr, atrf);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

        } catch (Exception e) {
                Logger.error(e.getMessage());
            }
        }, Akka.system().dispatcher());
    }

    private void sendFile() {
        Logger.debug("#########SEND FILE METHOD###########");
        FiniteDuration delay = FiniteDuration.create(0, TimeUnit.SECONDS);
        FiniteDuration frequency = FiniteDuration.create(1, TimeUnit.DAYS);

        Akka.system().scheduler().schedule(delay, frequency, () -> {
            try {
                JournalServiceImpl journalService = new JournalServiceImpl();
                journalService.getJournal();
                Utils.uploadFile();
            } catch (Exception e) {
                System.out.println(e);
            }
        }, Akka.system().dispatcher());
    }

    @Override
    public void onStop(Application app) {
        super.onStop(app);
        Logger.info("Stop app");
    }
}