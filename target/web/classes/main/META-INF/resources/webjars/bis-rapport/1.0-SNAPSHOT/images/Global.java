public class App extends GlobalSettings {

    /**
     * true => use login/password authentification method while sending mail
     * False => use anonumous authentification method
     */
    public static boolean LOGIN_PASSWD;

    public static String USERNAME;

    public static String SENDER;

    public static String PASSWORD;

    /**
     * Password used to encrypt attachment file
     */
    public static String ENCRYPT_PASSWORD;

//    public static String JUMIA_MAIL;

    public static String IMAP_HOST;

    public static String SMTP_HOST;

    public static int IMAP_PORT;

    public static int SMTP_PORT;

    /**
     * folder path of attachment files for received mail.
     */
    public static String INBOX_INPUT_FILE_PATH;

    /**
     * folder path of decrypted attachment files for received mail.
     */
    public static String INBOX_OUTPUT_FILE_PATH;

    /**
     * //Folder path of attachement file to send. encrypted
     */
    public static String TOSEND_OUTPUT_FILE_PATH;

    /**
     * Folder path of attachement file to send. Not encrypted
     */
    public static String TOSEND_INPUT_FILE_PATH;

    public static Store mStore = null;

    public static Session session = null;

    ArrayList<Contact> data = new ArrayList<Contact>();

    Statement st;
    Contact contact = new Contact();

    public void onStart(Application app) {
        Logger.info("Application has started");

        initAppSettings(app); //init settings located on the application.conf file

        getDatas(); //cron job that permit to check email every x time.
    }

    private void getDatas() {
        Connection connection = DB.getConnection();
        Date date = new Date();
        DateFormat outputFormatter = new SimpleDateFormat("MM-dd-yyyy");
        String output = outputFormatter.format(date);
        FiniteDuration delay = FiniteDuration.create(0, TimeUnit.SECONDS);
        FiniteDuration frequency = FiniteDuration.create(1, TimeUnit.DAYS);

        Akka.system().scheduler().schedule(delay, frequency, () -> {
            try {
                st= connection.createStatement();
                Journal journal = null;
                String req = "SELECT c.NUMEROCOMPTE,p.CONTACT FROM bgfimoney.tpecaisse AS c, bgfimoney.tpeboutique as b," +
                        "bgfimoney.partenaires AS p " +
                        "WHERE c.IDBOUTIQUE = b.IDBOUTIQUE " +
                        "AND b.IDPARTENAIRE = p.idpartenaire ";
                Logger.debug("########################### "+req);
                ResultSet rs1 = st.executeQuery(req);
                while (rs1.next()) {
                    String mail = rs1.getString("contact");
                    String nrocompte = rs1.getString("numerocompte");
                    contact = new Contact(mail,nrocompte);
                    data.add(contact);
                    Logger.debug("########################### "+contact);
                }
                Iterator<Contact> iterator = data.iterator();
                while (iterator.hasNext()) {
                    Contact c = new Contact();
                    c = iterator.next();
                    journal.getJournal(c.getNumerocompte());
                    MailHandler.sendEmail("Rapport", "Rapport journalier de la caisse "+c.getNumerocompte() , "ghambyte@gmail.com", "ghambyte@gmail.com", "/tmp/rapport-"+output+"/"+c.getNumerocompte()+"_rapport.csv", false);
                }
                //Generation du rapport annuel
                //rapportAnnuel();
            } catch (Exception e) {
                Logger.error(e.getMessage());
            }
        }, Akka.system().dispatcher());
    }

}